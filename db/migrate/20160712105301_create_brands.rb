class CreateBrands < ActiveRecord::Migration
  def change
    create_table :brands do |t|
      t.string :title, null: false
      t.string :scraping_source, null: false
      t.timestamps null: false
    end
  end
end
