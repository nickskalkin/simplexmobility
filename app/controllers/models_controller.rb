class ModelsController < ApplicationController
  def index
    find_brand
    scrap_phones_list

    render json: @models
  end

  private

  def find_brand
    @brand = Brand.find(params[:brand_id])
  end

  def scrap_phones_list
    @models = Scraper.new(@brand.scraping_source)
                                        .phone_models
  end
end
