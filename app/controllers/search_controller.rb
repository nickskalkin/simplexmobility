class SearchController < ApplicationController
  def search
    search_uri = "http://www.gsmarena.com/results.php3?sQuickSearch=yes&sName=#{params[:search_string]}"
    render json: Scraper.new(search_uri).search_items
  end
end
