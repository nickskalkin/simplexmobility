class PhonesController < ApplicationController
  def show
    render json: Scraper.new(params[:phone_url]).phone_info
  end
end
