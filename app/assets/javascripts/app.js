angular
  .module('simplexMobility', ['ui.router', 'templates'])
  .config([
    '$stateProvider', '$urlRouterProvider',
    function($stateProvider, $urlRouterProvider) {
      $stateProvider
        .state('home', {
          url: '/home',
          controller: 'PhoneInfoCtrl',
          resolve: {
            brandsPromise: ['brands', function(brands){
              return brands.fetchBrands();
            }]
          }
        });
      $urlRouterProvider.otherwise('home');
    }
  ])
  .factory('brands', [
    '$http',
    function($http){
      var o = {
        brands: []
      };
      o.fetchBrands = function() {
        return $http.get('/brands.json').success(function(data){
          angular.copy(data, o.brands);
        });
      }
    return o;
  }])
  .factory('models', [
    '$http',
    function($http){
      var o = {
        models: []
      };
      o.fetchPhoneModels = function(brand) {
        return $http.get('/brands/'+ brand.id + '/models.json').success(function(data){
          return data;
        });
      }
    return o;
  }])
  .factory('phones', [
    '$http',
    function($http){
      var o = {};
      o.fetchPhoneInfo = function(model) {
        return $http.get('/phone_info.json?phone_url=' + model.source).success(function(data){
          return data;
        });
      }
    return o;
  }])
  .factory('search', [
    '$http',
    function($http){
      var o = {};
      o.search = function(searchString) {
        return $http.get('/search.json?search_string=' + searchString).success(function(data){
          return data;
        });
      }
    return o;
  }])
  .controller('PhoneInfoCtrl', [
    '$scope', 'brands', 'models', 'phones',
    function($scope, brands, models, phones){
      $scope.title = 'Phone Info';
      $scope.brands = brands.brands;
      $scope.selectedBrand = $scope.brands[0];

      $scope.fetchPhoneModels = function() {
        models.fetchPhoneModels($scope.selectedBrand).then(function(data){
          $scope.phoneModels = data.data;
          $scope.selectedPhoneModel = $scope.phoneModels[0];
        });
      }

      $scope.fetchPhoneInfo = function() {
        phones.fetchPhoneInfo($scope.selectedPhoneModel).then(function(data){
          $scope.phoneInfo = data.data;
          console.log($scope.phoneInfo);
        });
      }
    }
  ])
  .controller('SearchPhoneCtrl', [
    '$scope', 'search',
    function($scope, search){
      $scope.title = 'Search Form';
      $scope.searchString = '';


      $scope.search = function() {
        search.search($scope.searchString).then(function(data){
          $scope.found_items = data.data;
        });
      }
    }
  ]);
