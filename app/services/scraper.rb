# Wrapper for scrapers, such as gsmarena
class Scraper
  attr_reader :url, :scraper, :page

  SCRAPPER_SOURCE_PATTERNS = {
    'gsmarena.com' => ::GsmarenaScraper
  }.freeze

  def initialize(url)
    @url = url
    detect_scraper
  end

  def brands
    scraper.brands
  end

  def phone_models
    scraper.phone_models
  end

  def phone_info
    scraper.phone_info
  end

  def search_items
    scraper.search_items
  end

  private

  def detect_scraper
    scraper_host = URI(url).host.gsub('www.', '')
    @scraper = SCRAPPER_SOURCE_PATTERNS[scraper_host].new(url)
  end
end
