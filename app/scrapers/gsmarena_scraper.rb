# scraper implementation for gsmarena
class GsmarenaScraper
  attr_reader :url, :page

  def initialize(url)
    @url = url
    initialize_mechanize_agent
  end

  def brands
    brands_collection.map do |item|
      {
        source: source_link_from_page(item['href']),
        title:  item.text
      }
    end
  end

  def phone_models
    phone_models_collection.map do |item|
      {
        source: source_link_from_page(item['href']),
        title:  item.search('strong span').first.children.text
      }
    end
  end

  def phone_info
    description = page.search('th').map do |th|
      category = th.parent.search('th').first.text
      subcategories = []
      th.parent.parent.search('td').each_slice(2) do |inner_th|
        subcategories.push(inner_th[0].text => inner_th[1].text)
      end
      { category: category, subcats: subcategories }
    end
    { title: phone_title, description: description }
  end

  def search_items
    search_collection.map do |item|
      {
        source:    source_link_from_page(item['href']),
        title:     item.search('strong span').first.children.text,
        thumbnail: item.search('img').first.attributes['src'].value
      }
    end
  end

  private

  def initialize_mechanize_agent
    agent = Mechanize.new
    @page = agent.get(url)
  end

  def source_link_from_page(uri)
    "#{page.uri.scheme}://#{page.uri.host}/#{uri}"
  end

  def brands_collection
    page.search('table td a').select.each_with_index { |_, i| i.odd? }
  end

  def phone_models_collection
    page.search('.makers ul a')
  end

  def phone_title
    page.search('.specs-phone-name-title').first.text
  end

  def search_collection
    page.search('.makers ul a')
  end
end
