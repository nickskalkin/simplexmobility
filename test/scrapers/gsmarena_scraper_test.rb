require 'test_helper'

class GsmarenaTest < ActiveSupport::TestCase
  def setup
    phone_models_stub = File.read(
      File.join(Rails.root, 'test', 'stubs', 'phone_models_stub')
    )
    FakeWeb.register_uri(
      :get, 'http://www.gsmarena.com/models',
      body: phone_models_stub, content_type: 'text/html'
    )
  end

  test 'should return correct phone models' do
    scraper = Scraper.new('http://www.gsmarena.com/models')
    assert_equal(
      scraper.phone_models.to_json.to_s,
      File.read(File.join(Rails.root, 'test', 'stubs', 'models_result'))
    )
  end
end
