require 'test_helper'

class ScraperTest < ActiveSupport::TestCase
  def setup
    FakeWeb.register_uri(
      :get, 'http://www.gsmarena.com/something',
      body: 'OK!', content_type: 'text/html'
    )
  end

  test 'should return valid scraper' do
    scraper = Scraper.new('http://www.gsmarena.com/something')
    assert_equal GsmarenaScraper, scraper.scraper.class
  end
end
